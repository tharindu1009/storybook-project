import gql from 'graphql-tag';


export const LOGIN = gql`
mutation Login( $userName: String!, $password: String!){
        login( userName:$userName, password: $password ){
            token
            user{
                id
                name
                email
                phoneNumber
                userName
                password
                userRole
                isActive
            }
        }
}
`
