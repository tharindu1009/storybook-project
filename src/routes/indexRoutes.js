import Login from '../screens/login/login';
import Home from '../screens/home/home';

const indexRoutes = [
    {
        path: "/login",
        name: "Login",
        icon: "",
        component: Login
    },
    {
        path: "/home",
        name: "Home",
        icon: "",
        component: Home
    }

];

export default indexRoutes;
