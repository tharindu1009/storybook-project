import React, { Component } from 'react';

class Button extends Component {

  render() {
    
    return (
      <div>
          {this.props.withLogin ? (
            <button type="button">Login</button>
          ):(
            <button type="button">Submit</button>
          )}
          
      </div>
    );
  }
}

export default Button;

