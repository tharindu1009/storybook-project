import React from 'react';
import {storiesOf} from '@storybook/react';
import Button from '../components/Button';
// import { action } from '@storybook/addon-actions';
// import { Button } from '@storybook/react/demo';


// export const TestButton = () => <Button id='login' name="login" text="Login" withLogin/>;

// import React from 'react';
// import {storiesOf} from '@storybook/react';
// import {Logo} from '../Logo/Logo';

storiesOf('TestButton', module)
    .add('default', () => <Button id='login' name="login"/>)
    .add('login', () => <Button id='login' name="login" withLogin />)

