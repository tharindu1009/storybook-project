import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus } from '../../actions/user-actions';

//query
import { LOGIN } from '../../queries/UserQueries';
//Spinner
import Loader from 'react-loader-spinner'

const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        }
    }
}

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: {
                form_state: 'default',
                title: '',
                message: '',
            },
            userName: "",
            password: "",
            loading: false
        };

        props.setFormStatus({ status: false, title: '', message: '' });

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        console.log(this.props)
    }

    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit = () => {

        this.props.setFormStatus({ status: false, title: '', message: '' });

        this.setState({ loading: true });
        const { userName, password } = this.state;

    
        if (!userName == '' || !password == '') {
            console.log(this.state);
            this.login().then(result => {

                localStorage.slbfeauthtoken = result.data.login.token;
                localStorage.USER_ID = result.data.login.user.id;
                localStorage.EMAIL = result.data.login.user.email;
                localStorage.USERNAME = result.data.login.user.userName;
                localStorage.NAME = result.data.login.user.name;
                localStorage.USER_ROLE = result.data.login.user.userRole;

                this.setState({ loading: false });

                this.props.history.push("/home")


            })
                .catch(error => {
                    if (error) {
                        console.log(error);
                        console.log('There is an error');
                        this.props.setFormStatus({ status: true, title: 'Oops!', message: 'Your username or password appeared to be wrong!' });
                    }
                    this.setState({ loading: false });

                });
        } else {
            this.props.setFormStatus({ status: true, title: 'Oops!', message: 'Required All Fields!' });
            this.setState({ userName: "", password: "", loading: false });
            return;
        }


    }

    login = async () => {
        const { userName, password } = this.state;
        console.log(this.props.client)
        const result = this.props.client.mutate(
            {
                mutation: LOGIN,
                variables: { userName, password },
            })
        return result;
    }

    redirectToHome = () => {
        this.props.history.push("/home");
    }


    render() {

        return (
            <div >
                <h2>Login Page</h2>
                <hr/>

                <button onClick={()=>this.redirectToHome()}>Home Screen</button>
         
            </div>


        )
    }
}

export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(Login))); 
