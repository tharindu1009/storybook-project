import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus } from '../../actions/user-actions';


const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        }
    }
}

class Home extends Component {
    constructor(props) {
        super(props);
       
    }

   

    render() {

        return (
            <div >
                <h2>Home Page</h2>
                <hr/>
         
            </div>


        )
    }
}

export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(Home))); 
